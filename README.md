# Accounting automation tools

These scripts are used to automate the May First accounting.

They are not meant to be used by others, except as a template to be copied and
then drastically modified.

For May Firsters: be sure to set these environment variables before trying to use them:

 * MF_CONTROL_PANEL_SSH_LOGIN
 * MF_CRM_SSH_LOGIN

These should be set to the username@server where the accounting downloaders
are located.
