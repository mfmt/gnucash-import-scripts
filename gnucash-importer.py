#!/usr/bin/python
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

"""
May First GnuCash Importer

This script queries our CiviCRM database at https://outreach.mayfirst.org/ for
recently added members, contributions, donations, and expenses and imports
those records into our GnuCash file to avoid manual double entry.

The query to our CiviCRM database uses our custom API and an API user key to
grant permissions. The user key should be placed in a text file in the HOME
directory of the user running this script (~/.config/mayfirst/outreach.api.txt).

Our CiviCRM database and GnuCash have entirely different concepts of entities.
Here's a simplified mapping:

CiviCRM                                     GnuCash
-------                                     -------
Contact                                     Customer
Contact External Identifier                 Customer Id
Membership                                  -
Contribution Id                             Invoice Id (plus 100,000)
Contribution Date                           Date in bank account register
Contribution Membership Term Start Date     Invoice date
Activity of the type Expense                Invoice and Payment
Activity Expense Date Spent                 Date of the expense invoice
Activity Expense Date Paid                  Date in the bank account register

Some notes:

1. For legacy reasons, the customer id matches the member id in the control
panel, not the contact id in the CiviCRM database. We store the member id
from the control panel in the External Identifier field in CiviCRM with a
member: prefix (e.g. member:1234).
2. We add an even 100,000 to the contribution id to create the invoice id.
This ensures we don't conflict with exiting invoice ids in GnuCash.
3. The Contribution Membership Term Start Date is a custom field that is
populated automatically when a membership payment is made and records
the accruel based accounting date that the payment is paying for.
"""

import sys
import os
import time
import warnings
from gnucash import Session, GncNumeric, Query, gnucash_business, gnucash_core
from gnucash import QOF_COMPARE_EQUAL, QOF_COMPARE_CONTAINS, INVOICE_TYPE, QOF_QUERY_AND, QOF_STRING_MATCH_NORMAL
from gnucash.gnucash_business import Customer, Invoice, Entry, BillTerm, Split, Account, Bill
from decimal import Decimal
import datetime
from dateutil.relativedelta import relativedelta
import requests
import json
import argparse
import shutil

LOG_LEVEL_INFO = 1
LOG_LEVEL_ERROR = 2
PRIORITY_INFO = 1
PRIORITY_ERROR = 2

class MayFirstGnuCashImporter():

    url = None
    api_key = None
    log_level = LOG_LEVEL_INFO
    since = None
    entity_id = None

    def print(self, msg, priority=PRIORITY_INFO, end="\n", flush=False):
        if self.log_level <= priority:
            print(msg, end=end, flush=flush)

    def main(self):
        # Path to the gnucash data file.
        gnucash_path_default = "{0}/Nextcloud/mayfirst-admin/accounting/gnucash/MFPL.gnucash".format(os.getenv('HOME'))

        # Path to the directory containing GnuCash data file backups.
        backup_path_default = "{0}/projects/mayfirst/gnucash/backup/".format(os.getenv('HOME'))

        #Allowed entities to import.
        allowed_entities = [ "Member", "Invoice", "Payment", "Donation", "Expense", "Refund" ]

        #URL of CiviCRM database.
        url_default = 'https://outreach.mayfirst.org/'

        s = None
        try:
            api_key_file = "{0}/.config/mayfirst/outreach.api.txt".format(os.getenv('HOME'))
            with open(api_key_file) as f:
                self.api_key = f.read().strip()

            if not self.api_key:
                self.print("Failed to find API key in file: {0}".format(api_key_file), priority=PRIORITY_ERROR)
                return False

            parser = argparse.ArgumentParser(description='Import financial information from the May First CiviCRM database.')
            parser.add_argument('--entities', help='What to import: Member, Invoice, Payment, Donation, Expense or empty for all, repeat as needed', action="append")
            parser.add_argument('--since', help='Restrict to entities older then the time frame specified (any strtotime recognized string, defaults to 3 months ago)')
            parser.add_argument('--gnucash-path', help='Path to the GnuCash data file')
            parser.add_argument('--backup-path', help='Path to the directory where a backup of the GnuCash data file will be copied, set to /dev/null to disable backup.')
            parser.add_argument('--id', help='If entering a single entity, specify the id of the entity')
            parser.add_argument('--url', help='URL to Accounting CiviCRM API.')
            parser.add_argument('--quiet', help='Suppress all output, default is False.', action='store_true')
            parser.set_defaults(gnucash_path=gnucash_path_default, backup_path=backup_path_default, url=url_default, quiet=False, entities=[])

            args = parser.parse_args()
            gnucash_path = args.gnucash_path
            entities = args.entities
            backup_path = args.backup_path
            self.entity_id = args.id
            self.url = "{0}civicrm/ajax/api4/MayfirstMember/Accounting".format(args.url)
            self.since = args.since

            if len(entities) == 0:
                entities = allowed_entities
            if len(entities) != 1 and self.entity_id:
                self.print("When passing an id, you must specify one and only one entity", priority=PRIORITY_ERROR)
                print(entities)
                return False

            if self.entity_id and self.since:
                self.print("You cannot pass both an id and a since value. The since value will be ignored.", priority=PRIORITY_ERROR)
                return False

            if args.quiet:
                self.log_level = LOG_LEVEL_ERROR

            if not os.path.exists(gnucash_path):
                self.print("Path to GnuCash data file ({0}) does not exist. Please pass it using the --gnucash argument.".format(gnucash_path), priority=PRIORITY_ERROR)
                return False

            if os.path.exists(gnucash_path + '.LCK'):
                self.print("Lock file exists. Is GNUCash running?", priority=PRIORITY_ERROR)
                return False

            for entity in entities:
                if not entity in allowed_entities:
                    self.print("Unknown entity: {0}".format(entity), priority=PRIORITY_ERROR)
                    return False

            if not os.path.exists(backup_path):
                self.print("Backup directory does not exist: {0}.".format(backup_path), priority=PRIORITY_ERROR)
                return False
            elif backup_path:
                if backup_path == "/dev/null":
                    self.print("Not backing up to /dev/null")
                else:
                    dest = "{0}/backup.{1}.gnucash".format(backup_path, datetime.date.today().strftime('%Y%m%d%H%M%s'))
                    shutil.copyfile(gnucash_path, dest)

            # Open for real now...
            self.print("Opening gnucash...")
            s = self.get_gnucash_session(gnucash_path)
            self.print("... done")

            for entity in entities:
                if entity == "Member":
                    self.import_members(s)
                if entity == "Invoice":
                    self.import_invoices(s)
                if entity == "Payment":
                    self.import_payments(s)
                if entity == "Donation":
                    self.import_donations(s)
                if entity == "Expense":
                    self.import_expenses(s)
                if entity == "Refund":
                    self.import_refunds(s)

            s.save()
            s.end()

            return True

        except Exception as e:
            if s:
                s.save()
                s.end()
            self.print("We hit an exception...", priority=PRIORITY_ERROR)
            raise e
            return False

    def member_exists(self, book, name, member_id):
        query = Query()
        query.search_for('gncCustomer')
        query.set_book(book)
        ret = False

        member_id = self.pad_number(member_id)

        for result in query.run():
            customer = gnucash_business.Customer(instance=result)
            if(customer.GetID() == member_id):
                ret = True
            if(customer.GetName() == name):
                ret = True

        query.destroy()
        return ret

    def get_data(self, entity, params={}):
        civi_params = {"entity": entity}
        if params:
            for key in params:
                civi_params[key] = params[key]
        else:
            if self.entity_id:
                civi_params['id'] = self.entity_id
            elif self.since:
                civi_params['date'] = self.since
        civi_params_json = json.dumps(civi_params)
        params = { "params": civi_params_json }

        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-Civi-Auth': "Bearer {0}".format(self.api_key)
        }

        r = requests.post(self.url, headers=headers, params=params)
        try:
            jsonOutput = r.json()
        except Exception as e:
            raise Exception("Error getting json output, text output is: {0}".format(r.text))
        if 'values' not in jsonOutput:
            if 'error_message' in jsonOutput:
                output = jsonOutput['error_message']
            else:
                output = jsonOutput
            raise Exception("CiviCRM returned no values for the {0} query. It returned: {1}".format(entity, output))
            
        return jsonOutput['values']

    def payment_exists(self, book, number, date):
        # We are comparing YYYY-mm-dd so stip off any time
        date = date.strftime("%Y-%m-%d")
        # Hopefully we won't receive two checks with the same check number
        # on the same date.
        query = Query()
        query.search_for('Trans')
        query.set_book(book)
        pred_data = gnucash_core.QueryStringPredicate(
            QOF_COMPARE_EQUAL, number, QOF_STRING_MATCH_NORMAL, False)
        query.add_term(['num'], pred_data, QOF_QUERY_AND)
        for transaction in query.run():
            trans = gnucash_business.Transaction(instance=transaction)
            trans_date = trans.GetDate()
            iso_date = trans_date.strftime('%Y-%m-%d')
            if trans.GetNum() == number and iso_date == date:
                return True
        return False

    def get_payments_for_invoice(self, book, invoice_id, date):
        # We are comparing YYYY-mm-dd so stip off any time
        date = date.strftime("%Y-%m-%d")
        query = Query()
        query.search_for('Trans')
        query.set_book(book)
        pred_data = gnucash_core.QueryStringPredicate(
          QOF_COMPARE_CONTAINS,
          f":{invoice_id}",
          QOF_STRING_MATCH_NORMAL,
          False
        )
        query.add_term(['num'], pred_data, QOF_QUERY_AND)
        hits = []
        for transaction in query.run():
            trans = gnucash_business.Transaction(instance=transaction)
            trans_date = trans.GetDate()
            iso_date = trans_date.strftime('%Y-%m-%d')
            if trans.GetNum().endswith(f":{invoice_id}") and iso_date == date:
                hits.append(trans) 
        return hits 

    def import_refunds(self, s):
        already_entered = 0
        entered = 0
        self.print("Entering refunds...")

        for row in self.get_data('Refund'):
            self.print(".", end="", flush=True)

            # Gather values.
            display_name = row[0]
            invoice_id = int(row[1])
            original_date = datetime.datetime.strptime(row[2], "%Y-%m-%d")
            original_amount = row[3]
            bank_id = int(row[4])
            original_number = row[5]
            # The list of payments made for this contribution (should be two, one is the
            # original, positive payment, the second is the negative refund.
            payments = row[6]

            # Set an identifier string to be used in case of errors.
            id_string = f"Invoice Id: {invoice_id}, Number: {original_number}, Date: {original_date}, Amount {original_amount}."

            if len(payments) != 2:
                print(f"{len(payments)} payments found during refund, expecting 2. I can't deal. {id_string}")
                continue

            # The refund will be in the payments row - which is a list.
            refund_balance = 0
            refund_amount = None
            refund_date = None
            refund_trxn = None

            for payment in payments:
                date = payment[0]
                amount = Decimal(payment[1])
                trxn = payment[2]
                if amount < 0:
                    # We found the refund.
                    refund_date = datetime.datetime.strptime(date, "%Y-%m-%d")
                    refund_amount = amount
                    refund_trxn = trxn 

                # Total the balance to ensure we are not dealing with a partial refund.
                refund_balance += amount

            # This is a reasonable problem - just report that we are not importing it.
            if refund_balance != 0:
                print(f"The two payments in the CiviCRM refund don't total zero. I can't deal with partial refunds. Sorry. {id_string}")
                continue

            # In contrast, these are errors.
            if refund_amount is None:
                raise Exception(f"Failed to find refund amount. {id_string}")
            if refund_date is None:
                raise Exception(f"Failed to find refund date. {id_string}")
            if refund_trxn is None:
                raise Exception(f"Failed to find refund trxn. {id_string}")

            # We search for existing transactions in GnuCash using only the invoice_id, because
            # the "number" (aka trxid) will change in CiviCRM if a refund is made.
            transactions = self.get_payments_for_invoice(s.book, invoice_id, original_date)
            transaction_count = len(transactions)

            # We expect either 1 transaction (if we have not yet recorded the
            # refund) or 2 transactions if we have recorded the refund.
            if len(transactions) == 0:
                raise Exception(f"No existing transaction for refund. {id_string}")
            if len(transactions) > 2:
                raise Exception(f"More than 2 transactions for refund. {id_string}")
            
            # We ensure that we have a second transaction in GnuCash to zero out the first one.
            if len(transactions) == 2:
                already_entered += 1
                # This is ok, already entered.
                continue

            # Add a new transaction for the refund.
            # Determine bank account.
            if bank_id == 5:
                bank_name = 'Stripe'
            elif bank_id == 2:
                bank_name = 'PayPal'
            elif bank_id == 1:
                bank_name = 'Checking Account'
            else:
                raise RuntimeError(f"Could not determine bank from {bank_id} in refund.")

            r_transaction = gnucash_business.Transaction(s.book)
            r_transaction.BeginEdit()

            root = s.book.get_root_account()
            commod_table = s.book.get_table()
            USD = commod_table.lookup('CURRENCY', 'USD')

            # Create splits - between bank/stripe/paypal and 
            # Membership Dues income account 
            income = root.lookup_by_name("Income")
            income_xfer_acc = income.lookup_by_name("Membership Dues - Combined")
            income_amount = self.gnc_numeric_from_decimal(abs(refund_amount))

            income_split = Split(s.book)
            income_split.SetValue(income_amount)
            income_split.SetAccount(income_xfer_acc)
            income_split.SetParent(r_transaction)

            assets = root.lookup_by_name("Assets")
            bank_xfer_acc = assets.lookup_by_name(bank_name)
            bank_amount = self.gnc_numeric_from_decimal(refund_amount)

            bank_split = Split(s.book)
            bank_split.SetValue(bank_amount)
            bank_split.SetAccount(bank_xfer_acc)
            bank_split.SetParent(r_transaction)

            # Save the transaction.
            description = f"Refund for {display_name}"
            r_transaction.SetCurrency(USD)
            r_transaction.SetDescription(description)
            r_transaction.SetNum(refund_trxn)
            r_transaction.SetDatePostedSecs(refund_date)
            r_transaction.CommitEdit()
            entered += 1

        self.print("")
        self.print("...done")
        self.print(f"Refunds entered: {entered} ({already_entered} already existed)")
        return True

    def import_payments(self, s):
        already_entered = 0
        entered = 0
        self.print("Entering payments...")
        for row in self.get_data('Payment'):
            invoice_id = int(row[0])
            payment_date = row[1]
            payment_amount = row[2]
            bank_id = int(row[3])
            # Combine payment identifier with invoice id so we can identify
            # which invoice it is paying and more accurately avoid double
            # payments.
            payment_identifier = "{0}:{1}".format(row[4], row[0])
            date = datetime.datetime.strptime(payment_date, "%Y-%m-%d")

            invoice = self.get_invoice(s.book, invoice_id)
            if invoice == False:
                raise Exception("Failed to find invoice with id {0}.".format(invoice_id))
            if(invoice.IsPaid() == False):
                txn = None
                root = s.book.get_root_account()
                commod_table = s.book.get_table()
                USD = commod_table.lookup('CURRENCY', 'USD')
                if self.payment_exists(s.book, payment_identifier, date):
                    already_entered += 1
                    continue
                assets = root.lookup_by_name("Assets")
                account_name = False
                if(bank_id == 1):
                    account_name = 'Checking Account'
                elif(bank_id == 5):
                    account_name = 'Stripe'
                elif(bank_id == 2):
                    account_name = 'PayPal'
                else:
                    raise Exception("Failed to find bank account name for id {0}.".format(bank_id))
                xfer_acc = assets.lookup_by_name(account_name)
                # I don't know what exch is supposed to be, but this works.
                exch = GncNumeric(0)
                memo = "Entered automatically by import script."
                #self.print("Entering payment ({0}, {1}, {2}, {3})".format(invoice_id, payment_identifier, payment_amount, payment_date))
                self.print(".", end="", flush=True)
                amount = self.gnc_numeric_from_decimal(Decimal(str(payment_amount)))
                invoice.ApplyPayment(txn, xfer_acc, amount, exch, date, memo, payment_identifier)
                entered += 1
            else:
                already_entered += 1
        self.print("")
        self.print("...done")
        self.print("Payments entered: {0} ({1} already existed)".format(entered, already_entered))
        return True

    def import_donations(self, s):
        book = s.book
        already_entered = 0
        entered = 0
        self.print("Entering donations...")
        for row in self.get_data('Donation'):
            payment_name = row[0]
            # 1 is citibank, 2 is paypal, 5 is stripe.
            payment_bank_id = int(row[1])
            payment_identifier = str(row[2])
            payment_date = row[3]
            date = datetime.datetime.strptime(payment_date, "%Y-%m-%d")
            payment_amount = row[4]

            if not payment_identifier:
                raise RuntimeError("Skipping payment from {0} for ${1} on {2} because no identifier".format(payment_name, payment_amount, payment_date))

            if payment_bank_id == 5:
                account_name = 'Stripe'
            elif payment_bank_id == 2:
                account_name = 'PayPal'
            elif payment_bank_id == 1:
                account_name = 'Checking Account'
            else:
                raise RuntimeError("Problem with {0} for ${1} on {2} because can't figure out which bank acount".format(payment_name, payment_amount, payment_date))

            if self.payment_exists(book, payment_identifier, date):
                already_entered += 1
                continue

            #self.print("Entering Donation: '{0}', {1}, {2}, {3}, {4}".format(date, payment_identifier, payment_amount, payment_name, account_name))
            self.print(".", end="", flush=True)
            transaction = gnucash_business.Transaction(book)
            transaction.BeginEdit()

            root = book.get_root_account()
            commod_table = book.get_table()
            USD = commod_table.lookup('CURRENCY', 'USD')

            # Create splits - between Stripe Assets account and
            # Income donations account.
            income = root.lookup_by_name("Income")
            income_xfer_acc = income.lookup_by_name("Donations")
            income_amount = self.gnc_numeric_from_decimal(Decimal('-{0}'.format(payment_amount)))

            income_split = Split(book)
            income_split.SetValue(income_amount)
            income_split.SetAccount(income_xfer_acc)
            income_split.SetParent(transaction)

            assets = root.lookup_by_name("Assets")
            assets_xfer_acc = assets.lookup_by_name(account_name)
            assets_amount = self.gnc_numeric_from_decimal(Decimal(payment_amount))

            assets_split = Split(book)
            assets_split.SetValue(assets_amount)
            assets_split.SetAccount(assets_xfer_acc)
            assets_split.SetParent(transaction)

            description = "Donation from " + payment_name
            transaction.SetCurrency(USD)
            transaction.SetDescription(description)
            transaction.SetNum(payment_identifier)
            transaction.SetDatePostedSecs(date)
            transaction.CommitEdit()

            entered += 1

        self.print("")
        self.print("...done")
        self.print("Donations entered: {0} ({1} already existed)".format(entered, already_entered))
        return True

    def import_invoices(self, s):
        entered = 0
        already_entered = 0
        self.print("Entering invoices...")
        for row in self.get_data('Invoice'):
            member_id = row[0]
            invoice_id = row[1]
            invoice_date = row[2]
            invoice_amount = row[3]
            # row[4] should be either annual or monthly.
            invoice_desc = "{0} Membership Dues".format(row[4])

            if not member_id.isdigit():
                # header line or not coded to a member?
                continue

            member_id = int(member_id)
            invoice_id = int(invoice_id)
            if self.invoice_exists(s.book, invoice_id):
                already_entered += 1
                continue

            # Ensure we have entered this member (if not, entering the invoice will fail).
            # If we have not entered the member and we can't figure out how to automatically
            # enter the member, then bail with an exception.
            if not self.member_exists(s.book, "No Name McGoo - fix me", member_id):
                # We have an invoice for a member that has not been imported into GnuCash. Try to find the name of the
                # member as entered in CiviCRM so we can import it.
                member = None
                print()
                print(f"Looking for missing member with control panel id {member_id}...")
                for row in self.get_data('Member', params={'controlPanelId': member_id}):
                    if int(row[1]) != member_id:
                        # Not sure how this would happen, but we don't want it.
                        continue
                    member = row[0]
                if member:
                    # Double check - this time with the proper name.
                    if self.member_exists(s.book, member, member_id):
                        raise Exception(f"Member {member} found in gnucash, but not with expected id {member_id}.")
                    else:
                        # Enter it
                        print()
                        print(f"Entering missing member {member}")
                        self.member_enter(s, member, member_id)
                else:
                    raise Exception(f"Control Panel Member id {member_id}, invoice id {invoice_id} - member doesn't exist in GnuCash. Try looking up the member and importing with --id (but set --id to the contact id in CiviCRM)")
            # We are all good! Enter the invoice.
            # self.print("{0}, {1}, {2}, {3}, {4}".format(member_id, invoice_id, invoice_date, invoice_amount, invoice_desc))
            self.invoice_enter(s, member_id, invoice_id, invoice_date, invoice_amount, invoice_desc)
            entered += 1
        self.print("")
        self.print("...done")
        self.print("Invoices entered: {0} ({1} already existed)".format(entered, already_entered))

    def import_expenses(self, s):
        already_entered = 0
        entered = 0
        self.print("Entering expenses...")
        for row in self.get_data('Expense'):
            # Convert this-is-my-account to This Is My Account
            # Also, Avoid capitalized And and Tls should be TLS
            if not row[0]:
                print(row)
            account = row[0].replace('_', ' ')
            date_spent = row[1]
            date_paid = row[2]
            number = row[3]
            desc =  row[4]
            bank = row[5]
            amount = row[6]

            if self.validate_account(s, account) == False:
                raise Exception("Failed to validate account {0}".format(account))
            if self.validate_date(date_spent) == False:
                raise Exception("Failed to validate date spent: {0}".format(date_spent))
            if self.validate_date(date_paid) == False:
                raise Exception("Failed to validate date paid: {0}".format(date_paid))
            if self.validate_decimal(amount) == False:
                raise Exception("Failed to validate amount paid: {0}".format(amount))

            # Convert from string to decimal so gnucash can handle it.
            amount = Decimal(amount).quantize(Decimal('.01'))

            if self.expense_exists(s, number, date_spent):
                already_entered += 1
            else:
                self.enter_expense(s, account, date_spent, date_paid, number, desc, bank, amount)
                entered += 1

        self.print("")
        self.print("...done")
        self.print("Expenses entered: {0} ({1} already existed)".format(entered, already_entered))

    def enter_expense(self, s, account, date_spent, date_paid, number, desc, bank, amount):
        # We have to create some gnucash entities with our values.
        asset_account_str = None
        if bank == "Citibank":
            asset_account_str = 'Checking Account'
        else:
            asset_account_str = 'PayPal'

        root = s.book.get_root_account()
        assets = root.lookup_by_name("Assets")
        asset_account = assets.lookup_by_name(asset_account_str)
        gnucash_account = root.lookup_by_name(account)

        trans = gnucash_business.Transaction(s.book)
        trans.BeginEdit()
        split1 = Split(s.book)
        split1.SetAccount(gnucash_account)
        split1.SetValue(self.gnc_numeric_from_decimal(amount))
        split1.SetParent(trans)

        split2 = Split(s.book)
        split2.SetAccount(asset_account)
        split2.SetValue(self.gnc_numeric_from_decimal(-amount))
        split2.SetParent(trans)

        comm_table = s.book.get_table()
        usd = comm_table.lookup("CURRENCY", "USD")
        trans.SetCurrency(usd)
        trans.SetDescription(desc)
        trans.SetNum(number)
        gnucash_date = datetime.datetime.strptime(date_spent, "%Y-%m-%d")
        trans.SetDatePostedSecs(gnucash_date)
        trans.CommitEdit()

    def expense_exists(self, s, number, date_spent):
        date = datetime.datetime.strptime(date_spent, "%Y-%m-%d")
        # We are comparing YYYY-mm-dd so stip off any time
        date = date.strftime("%Y-%m-%d")
        # Hopefully we won't receive two checks with the same check number
        # on the same date.
        query = Query()
        query.search_for('Trans')
        query.set_book(s.book)
        pred_data = gnucash_core.QueryStringPredicate(
            QOF_COMPARE_EQUAL, number, QOF_STRING_MATCH_NORMAL, False)
        query.add_term(['num'], pred_data, QOF_QUERY_AND)
        for transaction in query.run():
            trans = gnucash_business.Transaction(instance=transaction)
            trans_date = trans.GetDate()
            iso_date = trans_date.strftime('%Y-%m-%d')
            if trans.GetNum() == number and iso_date == date:
                return True
        return False

    def validate_account(self, s, account):
        account = s.book.get_root_account().lookup_by_name(account)
        if account is None:
            return False
        return True

    def validate_date(self, date):
        try:
            datetime.datetime.strptime(date, "%Y-%m-%d")
        except:
            return False
        return True

    def validate_decimal(self, val):
        try:
            value = Decimal(val).quantize(Decimal('.01'))
        except:
            return False
        if value is None:
            return False
        return True

    def import_members(self, s):
        already_entered = 0
        entered = 0
        self.print("Entering members...")
        for row in self.get_data('Member'):
            member = row[0]
            member_id = int(row[1])
            if self.member_exists(s.book, member, member_id):
                #self.print("Already entered {0} ({1})".format(member, member_id))
                already_entered += 1
            else:
                self.member_enter(s, member, member_id )
                entered += 1

        self.print("")
        self.print("...done")
        self.print("Members entered: {0} ({1} already existed)".format(entered, already_entered))

    def get_gnucash_session(self, path):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            return Session(path, is_new = False)

    def member_enter(self, s, member, member_id):
        self.print("Entering {0} with id {1}".format(member, member_id))
        book = s.book
        root = book.get_root_account()
        commod_table = book.get_table()
        USD = commod_table.lookup('CURRENCY', 'USD')
        member_id = self.pad_number(member_id)
        new_customer = Customer(book, member_id, USD, member)

    def invoice_enter(self, s, member_id, invoice_id, invoice_date, invoice_amount, invoice_desc):
        book = s.book
        root = book.get_root_account()
        commod_table = book.get_table()
        USD = commod_table.lookup('CURRENCY', 'USD')

        invoice_id = self.pad_number(invoice_id)
        member_id = self.pad_number(member_id)
        invoice_date_object = datetime.datetime.strptime(invoice_date, "%Y-%m-%d")
        invoice_date_due_object = invoice_date_object + relativedelta(days=1)

        member = book.CustomerLookupByID(member_id)

        assert( member != None )
        assert( isinstance(member, Customer) )

        assets = root.lookup_by_name("Assets")
        receivable = assets.lookup_by_name("Accounts Receivable")
        if(receivable == None):
            raise Exception("Failed to create receivable object")

        income = root.lookup_by_name("Membership Dues - Combined")
        invoice_value = self.gnc_numeric_from_decimal(Decimal(str(invoice_amount)))
        invoice = Invoice(book=book, id=invoice_id, currency=USD, owner=member, date_opened=invoice_date_object )

        invoice_entry = Entry(book, invoice)
        invoice_entry.SetDescription(invoice_desc)
        invoice_entry.SetQuantity( GncNumeric(1) )
        invoice_entry.SetInvAccount(income)
        invoice_entry.SetInvPrice(invoice_value)
        invoice_entry.SetDate(invoice_date_object)
        invoice_entry.SetDateEntered(invoice_date_object)

        #self.print("Entering invoice for member {0}: {1} {2} {3}".format(member_id, invoice_id, invoice_date, invoice_amount))
        self.print(".", end="", flush=True)
        invoice.PostToAccount(receivable, invoice_date_object, invoice_date_due_object, "", True, False)
        invoice.SetDatePosted(invoice_date_object)

    def pad_number(self, num):
        return "%(number)06d" % { 'number': num }

    def invoice_exists(self, book, invoice_id):
        invoice = self.get_invoice(book, invoice_id)
        if(invoice == False):
            return False
        return True

    def get_invoice(self, book, invoice_id):
        query = Query()
        query.search_for('gncInvoice')
        query.set_book(book)

        # return only invoices (1 = invoices)
        pred_data = gnucash_core.QueryInt32Predicate(QOF_COMPARE_EQUAL, 1)
        query.add_term([INVOICE_TYPE], pred_data, QOF_QUERY_AND)

        invoice_id = self.pad_number(invoice_id)
        for result in query.run():
            invoice = gnucash_business.Invoice(instance=result)
            if(invoice.GetID() == invoice_id):
                return invoice
        return False

    def gnc_numeric_from_decimal(self, decimal_value):
            sign, digits, exponent = decimal_value.as_tuple()

            # convert decimal digits to a fractional numerator
            # equivlent to
            # numerator = int(''.join(digits))
            # but without the wated conversion to string and back,
            # this is probably the same algorithm int() uses
            numerator = 0
            TEN = int(Decimal(0).radix()) # this is always 10
            numerator_place_value = 1
            # add each digit to the final value multiplied by the place value
            # from least significant to most sigificant
            for i in range(len(digits)-1,-1,-1):
                    numerator += digits[i] * numerator_place_value
                    numerator_place_value *= TEN

            if decimal_value.is_signed():
                    numerator = -numerator

            # if the exponent is negative, we use it to set the denominator
            if exponent < 0 :
                    denominator = TEN ** (-exponent)
            # if the exponent isn't negative, we bump up the numerator
            # and set the denominator to 1
            else:
                    numerator *= TEN ** exponent
                    denominator = 1

            # print("original: {0} and args are {1} and {2}".format(decimal_value, numerator, denominator))
            return GncNumeric(numerator, denominator)

if __name__ == "__main__":
    importer = MayFirstGnuCashImporter()
    ret = importer.main()
    if ret == False:
        sys.exit(1)

