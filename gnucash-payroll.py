#!/usr/bin/python
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

import sys 
import os
import warnings
from gnucash import Session, GncNumeric
from gnucash.gnucash_business import Split, Transaction
from decimal import Decimal
import datetime
import traceback

def main():
    path = f"{os.getenv('HOME')}/Nextcloud/mayfirst-admin/accounting/gnucash/MFPL.gnucash"
    if not os.path.exists(path):
        print(f"Path ({path}) does not exist. Please set MFPL_GNUCASH_PATH environment variable")
        return 1 

    if os.path.exists(path + '.LCK'):
        print("Lock file exists. Is GNUCash running?")
        return 1 
        
    # Open for real now...
    print("Opening GnuCash...")
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        s = Session(path, is_new = False)

    cont = True
    default_date = datetime.datetime.now().strftime("%Y-%m-15")
    while cont:
        p = payroller()
        p.default_date = default_date
        p.s = s
        p.book = s.book
        p.root = s.book.get_root_account()
        try:
            cont = p.run()
        except:
            print("Something went wrong. Here's the tracebook")
            print(traceback.format_exc())
            print("Now saving...")
            p.exit()
        default_date = p.default_date
    print("Saving GnuCash...")
    p.exit()

class payroller:
    s = None
    book = None 
    root = None 
    default_date = None

    def exit(self):
        self.s.save()
        self.s.end()

    def prompt(self, prompt, validate = None, default = ""):
        ret = input(prompt + " (type quit to exit) [" + default + "]: ")
        if ret == "":
            ret = default
        if ret == "quit":
            return "quit" 
        if validate is not None:
            if not validate(ret):
                print("Not a valid option, try again.")
                return self.prompt(prompt, validate)
        return ret

    def validate_account(self, account):
        account = self.root.lookup_by_name(account)
        if account is None:
            return False
        return True

    def validate_date(self, date):
        try:
            datetime.datetime.strptime(date, "%Y-%m-%d")
        except:
            return False
        return True
    
    def validate_employee(self, employee):
        if employee == 'Jaime Villarreal':
            return True
        return False 

    def validate_decimal(self, val):
        if not val:
            return False
        try:
            value = Decimal(val).quantize(Decimal('.01'))
        except:
            return False
        if value is None:
            return False
        return True

    def defaults(self, employee, key):
        # First, define the defaults dictionary
        defaults = { 
                'Jaime Villarreal': {
                    'fica': '201.50',
                    'medicaid': '47.13',
                    'unemployment': '0',
                    'futa': '0',
                    'nyrs': '0',
                    'salary': '3250',
                    'employee_deductions': '0'
                }
        }
        try:
            return defaults[employee][key]
        except:
            return 0

    def run(self):
        employee = self.prompt("Please enter the employee name", self.validate_employee, 'Jaime Villarreal')
        if employee == "quit":
            return False

        # Set default date to today's date, mostly so the user can see the proper format.
        payroll_date_str = self.prompt("Please enter the Payroll Date (YYYY-MM-DD)", self.validate_date, self.default_date)
        # Reset default date so it can be re-used.
        self.default_date = payroll_date_str
        if payroll_date_str == "quit":
            return False
        
        print("When entering amounts, enter 0 if no amount is listed.")

        salary = self.prompt("Please enter the Salary amount", self.validate_decimal, self.defaults(employee, 'salary'))
        if salary == "quit":
            return False
        salary = Decimal(salary)

        """

        Stuff only needed in NY
        nyrs = Decimal(self.prompt("Please enter the NYRS amount", self.validate_decimal, self.defaults(employee, 'nyrs')))
        if nyrs == "quit":
            return False
        nyrs = Decimal(nyrs)

        futa = Decimal(self.prompt("Please enter the FUTA amount", self.validate_decimal, self.defaults(employee, 'futa')))
        if futa == "quit":
            return False
        futa = Decimal(futa)

        unemployment = self.prompt("Please enter the unemployment amount", self.validate_decimal, self.defaults(employee, 'unemployment'))
        if unemployment == "quit":
            return False
        unemployment = Decimal(unemployment)

        """
        nyrs = Decimal(0)
        futa = Decimal(0)
        unemployment = Decimal(0)

        medicaid = self.prompt("Please enter the medicaid amount", self.validate_decimal, self.defaults(employee, 'medicaid'))
        if medicaid == "quit":
            return False
        medicaid = Decimal(medicaid)

        fica = self.prompt("Please enter the FICA amount", self.validate_decimal, self.defaults(employee, 'fica'))
        if fica == "quit":
            return False
        fica = Decimal(fica)
        
        # Both employees and employers are responsible for the NY state
        # insurance and disability fund.  Our payroll company lists, but does
        # not pay out, the employee contribution. We pay the full amount
        # ourselves on a different accounting line (we are not keeping track
        # of how much is our responsibility and how much is the employee
        # responsibility). Since this amount is not deducted from our
        # checking account and not sent to the employee, we have to track
        # it here so we can deduct it from the salary line below.
        employee_deductions = self.prompt("Please enter the Employee Deductions", self.validate_decimal, self.defaults(employee, 'employee_deductions'))
        if salary == "quit":
            return False
        employee_deductions = Decimal(employee_deductions)

        total_benefits = fica + medicaid + futa + unemployment + nyrs
        total_withdrawal = total_benefits + salary
        adjusted_salary = salary - employee_deductions
        asset_account_str = "Checking Account"
        asset_account = self.root.lookup_by_name(asset_account_str)

        # Confirm.
        print("-----")
        print("-----")
        print("Employee: {0}".format(employee))
        print("Payroll Date: {0}".format(payroll_date_str))
        """
        print("Futa: {0}".format(futa))
        print("NYRS: {0}".format(nyrs))
        print("Unemployment: {0}".format(unemployment))
        """
        print("FICA: {0}".format(fica))
        print("Medicaid: {0}".format(medicaid))
        print("Total Employer Taxes: {0}".format(total_benefits))
        print("Salary: {0}".format(salary))
        print("Adjusted Salary: {0}".format(adjusted_salary))
        print("Employee deductions: {0}".format(employee_deductions))
        print("Total Withdrawal: {0}".format(total_withdrawal))
        print("-----")
        print("-----")
        
        gnucash = input("Enter into gnuCash? [Yn] ")
        if gnucash == "n":
            print("Not entering into gnuCash.")
        else:
            g_fica = gnc_numeric_from_decimal(fica)
            g_medicaid = gnc_numeric_from_decimal(medicaid)
            g_unemployment = gnc_numeric_from_decimal(unemployment)
            g_nyrs = gnc_numeric_from_decimal(nyrs)
            g_futa = gnc_numeric_from_decimal(futa)
            g_salary = gnc_numeric_from_decimal(salary)
            g_adjusted_salary = gnc_numeric_from_decimal(adjusted_salary)
            g_employee_deductions = gnc_numeric_from_decimal(employee_deductions)
            assets = self.root.lookup_by_name("Assets")
            asset_account = assets.lookup_by_name(asset_account_str)
            payroll_date = datetime.datetime.strptime(payroll_date_str, "%Y-%m-%d")

            comm_table = self.book.get_table()
            usd = comm_table.lookup("CURRENCY", "USD")

            futa_expense_account = self.root.lookup_by_name("FUTA")
            unemployment_expense_account = self.root.lookup_by_name("Unemployment Insurance")
            nyrs_expense_account = self.root.lookup_by_name("NY RS")
            fica_expense_account = self.root.lookup_by_name("Employer FICA")
            medicaid_expense_account = self.root.lookup_by_name("Employer Medicaid")
            salary_expense_account = self.root.lookup_by_name("Salaries")

            trans = Transaction(self.book)
            trans.BeginEdit()
            
            if fica > 0:
                split1 = Split(self.book)
                split1.SetAccount(fica_expense_account)
                split1.SetValue(g_fica)
                split1.SetParent(trans)

            if medicaid > 0:
                split2 = Split(self.book)
                split2.SetAccount(medicaid_expense_account)
                split2.SetValue(g_medicaid)
                split2.SetParent(trans)

            if unemployment > 0:
                split3 = Split(self.book)
                split3.SetAccount(unemployment_expense_account)
                split3.SetValue(g_unemployment)
                split3.SetParent(trans)

            if nyrs > 0:
                split4 = Split(self.book)
                split4.SetAccount(nyrs_expense_account)
                split4.SetValue(g_nyrs)
                split4.SetParent(trans)

            if futa > 0:
                split5 = Split(self.book)
                split5.SetAccount(futa_expense_account)
                split5.SetValue(g_futa)
                split5.SetParent(trans)

            if adjusted_salary > 0:
                split6 = Split(self.book)
                split6.SetAccount(salary_expense_account)
                split6.SetValue(g_adjusted_salary)
                split6.SetParent(trans)

            split8 = Split(self.book)
            split8.SetAccount(asset_account)
            split8.SetValue(gnc_numeric_from_decimal(-total_withdrawal))
            split8.SetParent(trans)

            trans.SetCurrency(usd)
            trans.SetDescription("Payroll for " + employee)
            trans.SetNum("Direct Deposit")

            trans.SetDatePostedSecs(payroll_date)
            trans.CommitEdit()

            print("Entered Payroll")
            self.s.save()
        return True

def gnc_numeric_from_decimal(decimal_value):
        sign, digits, exponent = decimal_value.as_tuple()

        # convert decimal digits to a fractional numerator
        # equivlent to
        # numerator = int(''.join(digits))
        # but without the wated conversion to string and back,
        # this is probably the same algorithm int() uses
        numerator = 0
        TEN = int(Decimal(0).radix()) # this is always 10
        numerator_place_value = 1
        # add each digit to the final value multiplied by the place value
        # from least significant to most sigificant
        for i in range(len(digits)-1,-1,-1):
                numerator += digits[i] * numerator_place_value
                numerator_place_value *= TEN

        if decimal_value.is_signed():
                numerator = -numerator

        # if the exponent is negative, we use it to set the denominator
        if exponent < 0 :
                denominator = TEN ** (-exponent)
        # if the exponent isn't negative, we bump up the numerator
        # and set the denominator to 1
        else:
                numerator *= TEN ** exponent
                denominator = 1

        return GncNumeric(numerator, denominator)

if __name__ == "__main__":
    sys.exit(main())

